package com.example.zuitt.Application.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.example.zuitt.Application.models.User;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {
}
